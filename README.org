IMPORTANT: *This is a WIP*

* es-querystring-parser

An attempt to build a nodejs parser for ES query string syntax

There are two main modules:

- ~parser~: which is in charge of taking a ES query string and tranform it into an intermediate representation.
- ~rewriter~: which takes an intermediate representation and generates a ES json query, ready to be used.

** Default field queries

The default field queries (which normally are perform against ~_all~, if
enabled), doesn't really have a way to be represented in a json query, so if
your query may contain this kind of queries it would be better to provide a
transformation to handle them, here is an example:

#+BEGIN_SRC javascript
    const transform = (expr) => {
      switch (expr.query_type) {
      case 'all':
        return { query_type: 'match', field: 'lastname', value: expr.value };
      default:
        return expr;
      }
    };

    rewriter.rewrite({ transform }, repr);
#+END_SRC

** Wildcard queries

The wildcard json queries are suppose to run against non analyzed fields, if
your querystring may contain a wildcard over an analyzed field, it might be a
good idea to provide a transformation to normalize the provided string.

(In my experience it might be a good idea to lower case them).

** Missing features

- support for: ~title:(quick OR brown)~ ~title:(quick brown)~
- support for ~age:(>=10 AND <20)~ ~age:(+>=10 +<20)~
- support for [[https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#_boosting][boosting]]
- support for ~book.\\*: "Smith"~ in the rewriter (doesn't seem to have a representation in json query strings)

** Running tests with docker

*** Unit tests:

#+BEGIN_EXAMPLE
docker-compose run es-querystring-parser npm i
docker-compose run --no-deps --rm es-querystring-parser npm test
#+END_EXAMPLE

*** Integration tests:

#+BEGIN_EXAMPLE
docker-compose up -d elasticsearch
docker-compose run es-querystring-parser npm i
docker-compose run --no-deps --rm es-querystring-parser npm run integration-tests
#+END_EXAMPLE

*** To start kibana:

#+BEGIN_EXAMPLE
docker-compose up -d kibana
#+END_EXAMPLE

And then open [[http://localhost:5601/][http://localhost:5601/]] in your browser.
