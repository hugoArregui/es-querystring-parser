const _             = require('lodash');
const elasticsearch = require('elasticsearch');
const expect        = require('chai').expect;
const parser        = require('../../lib/parser');
const rewriter      = require('../../lib/rewriter');

const util = require('util');

const TYPE = 'test';

const esClient = new elasticsearch.Client({
  apiVersion: '5.5',
  hosts: 'elasticsearch:9200'
});

function bulk(esClient, index, resources) {
  const body = [];

  resources.forEach((r) => {
    body.push({ index: { _index: index, _type: TYPE, _id: r._id } });
    body.push(r._source);
  });

  return esClient
    .bulk({
      refresh: true,
      body: body
    });
}

describe("custom rewriter", () => {

  const docs = {
    r1: {
      _id: '1',
      _source: {
        name: 'John',
        age: 30,
        code: '10',
        book: {
          title: 'History of something',
          author: 'Jane'
        }
      }
    },
    r2: {
      _id: '2',
      _source: {
        name: 'Peter',
        age: 40,
        code: '11',
        book: {
          title: 'History of something else',
          author: 'Smith'
        }
      }
    },
    r3: {
      _id: '3',
      _source: {
        name: 'Peter',
        lastname: 'Stevenson',
        book: {
          title: 'History of something else',
          author: 'Smith',
          isbn: '33'
        }
      }
    },
    r4: {
      _id: '4',
      _source: {
        name: 'Carlos',
        lastname: 'Peter',
        age: 20
      }
    }
  };

  const index = 'rewriter_test';

  before(() => bulk(esClient, index, _.values(docs)));

  class Boom extends Error {
    constructor(...params) {
      super(...params);
    }
  }

  function search(qs) {
    const repr = parser.parse(qs);

    const transform = (expr) => {
      switch (expr.query_type) {
      case 'regexp':
        throw new Boom();
      case 'all':
        return { query_type: 'match', field: 'lastname', value: expr.value };
      default:
        return expr;
      }
    };

    const esQuery = rewriter.rewrite({ transform }, repr);
    return esClient
      .search({
        index: index,
        type: TYPE,
        body: {
          query: esQuery
        }
      });
  }

  it("reject regexp queries", () => {
    expect(() => search('f:/foo/'))
      .to.throw(Boom);
  });

  describe("rewrite all query", () => {
    it('should search only in lastname field', () => {
      return search('"Peter"')
        .then((r) => {
          expect(r.hits.total).to.equal(1);
          expect(r.hits.hits[0]._id).to.equal(docs.r4._id);
        });
    });
  });
});
