const elasticsearch = require('elasticsearch');
const Promise       = require('bluebird');
const expect        = require('chai').expect;
const parser        = require('../../lib/parser');
const rewriter      = require('../../lib/rewriter');

const util = require('util');

const TYPE = 'test';

const esClient = new elasticsearch.Client({
  apiVersion: '5.5',
  hosts: 'elasticsearch:9200'
});

function bulk(esClient, index, resources) {
  const body = [];

  resources.forEach((r) => {
    body.push({ index: { _index: index, _type: TYPE, _id: r._id } });
    body.push(r._source);
  });

  return esClient
    .bulk({
      refresh: true,
      body: body
    });
}

function searchByQs(esClient, index, qs) {
  return esClient
    .search({
      index: index,
      type: TYPE,
      body: {
        query: {
          query_string: {
            query: qs
          }
        }
      }
    });
}

function searchByQuery(esClient, index, query) {
  // console.log(util.inspect(query, {depth: null}));
  return esClient
    .search({
      index: index,
      type: TYPE,
      body: {
        query
      }
    });
}

function search(esClient, index, qs, rewriterOpts = {}) {
  const repr = parser.parse(qs);
  const esQuery = rewriter.rewrite(rewriterOpts, repr);
  return Promise
    .all([
      searchByQs(esClient, index, qs),
      searchByQuery(esClient, index, esQuery)
    ])
    .spread((qsResult, rewriteResult) => {
      const total = qsResult.hits.total;
      const ids = qsResult.hits.hits.map((h) => h._id);
      return [total, ids, qsResult, rewriteResult];
    });
}

describe("parse and rewrite", () => {

  const r1 = {
    _id: '1',
    _source: {
      name: 'John',
      age: 30,
      code: '10',
      book: {
        title: 'History of something',
        author: 'Jane'
      },
      birth_date: new Date(1980, 0, 1)
    }
  };

  const r2 = {
    _id: '2',
    _source: {
      name: 'Peter',
      age: 40,
      code: '11',
      book: {
        title: 'History of something else',
        author: 'Smith'
      },
      birth_date: new Date(1900, 0, 1)
    }
  };

  const r3 = {
    _id: '3',
    _source: {
      name: 'Peter',
      lastname: 'Stevenson',
      book: {
        title: 'History of something else',
        author: 'Smith',
        isbn: '33'
      },
      birth_date: new Date(2001, 0, 1)
    }
  };

  const r4 = {
    _id: '4',
    _source: {
      name: 'Carlos',
      age: 20
    }
  };


  const r5 = {
    _id: '5',
    _source: {
      name: 'Jane',
      age: 30
    }
  };

  describe("match none", () => {
    const index = 'match_none_test';

    before(() => {
      return bulk(esClient, index, [r1, r2, r3, r4]);
    });

    it("match", () => {
      const qs = '';
      return search(esClient, index, qs)
        .spread((total) => {
          expect(total).to.equal(0);
        });
    });
  });

  describe("match queries", () => {
    const index = 'match_queries_test';

    before(() => {
      return bulk(esClient, index, [r1, r2]);
    });

    it("match", () => {
      const qs = 'name:John';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r1._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("match for nested property", () => {
      const qs = 'book.author: Jane';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r1._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("numeric match", () => {
      const qs = 'age: 30';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r1._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("match phrase", () => {
      const qs = 'name:"John"';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r1._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("match phrase for nested property", () => {
      const qs = 'book.title: "History of something"';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r2._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });
  });

  describe("match wildcards", () => {
    const index = 'wildcard_test';

    before(() => bulk(esClient, index, [r1, r2, r3, r4, r5]));

    it("match with *", () => {
      const qs = 'name.keyword:J*';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r5._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });
  });


  describe("default operator queries", () => {
    const index = 'default_operator_test';

    before(() => bulk(esClient, index, [r1, r2, r3]));

    it("exists queries", () => {
      const qs = 'age:30 age:40';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r2._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });
  });

  describe("exists queries", () => {
    const index = 'exists_queries_test';

    before(() => bulk(esClient, index, [r1, r2, r3]));

    it("property exists", () => {
      const qs = '_exists_: lastname';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r3._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("property exists - with quotes", () => {
      const qs = '_exists_: "lastname"';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r3._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("nested property exists", () => {
      const qs = '_exists_: book.isbn';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r3._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });
  });

  describe("comparators", () => {
    const index = 'comparators_queries_test';

    before(() => bulk(esClient, index, [r1, r2, r3, r4]));

    it("greater", () => {
      const qs = 'age:>30 ';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r2._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("greater or equals", () => {
      const qs = 'age:>=30 ';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r2._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("lesser", () => {
      const qs = 'age:<40 ';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r4._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("lesser or equals", () => {
      const qs = 'age:<=40 ';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(3);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r2._id);
          expect(ids).to.include(r4._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("[range]", () => {
      const qs = 'age:[20 TO 40]';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(3);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r2._id);
          expect(ids).to.include(r4._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("(range)", () => {
      const qs = 'age:{20 TO 40}';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r1._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("[range)", () => {
      const qs = 'age:[20 TO 40}';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r4._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it("(range]", () => {
      const qs = 'age:{20 TO 40]';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r2._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });


    it("[range with date literals]", () => {
      const qs = 'birth_date:[1900-01-01 TO 2000-01-01]';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r2._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });
  });

  describe('boolean', () => {
    const index = 'boolean_test';

    before(() => bulk(esClient, index, [r1, r2, r3, r4]));

    it('simple OR', () => {
      const qs = 'code: "10" OR code:"11"';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r1._id);
          expect(ids).to.include(r2._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it('simple AND', () => {
      const qs = 'code: "10" AND name:John';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r1._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it('combining `and` and `or` operators', () => {
      const qs = 'age:40 OR name:John AND age:30';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r1._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it('combining `and` and `or` operators - explicit', () => {
      const qs = '(age:40 OR name:John) AND age:30';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r1._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });
  });


  describe('bool operators', () => {
    const index = 'bool_operator_test';

    before(() => bulk(esClient, index, [r1, r2, r3, r4]));

    it('single operator', () => {
      const qs = '+name:Peter';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(2);
          expect(ids).to.include(r2._id);
          expect(ids).to.include(r3._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it('combining must operators', () => {
      const qs = '+name:Peter +name:John';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(0);
          expect(ids).to.have.length(0);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });

    it('combining must and must_not operators', () => {
      const qs = '+name:Peter -lastname:Stevenson';
      return search(esClient, index, qs)
        .spread((total, ids, qsResult, rewriteResult) => {
          expect(total).to.equal(1);
          expect(ids).to.include(r2._id);
          expect(qsResult.hits).to.deep.equal(rewriteResult.hits);
        });
    });
  });
});
