const elasticsearch = require('elasticsearch');
const esClient = new elasticsearch.Client({
  apiVersion: '5.5',
  hosts: 'elasticsearch:9200'
});

esClient
  .indices
  .delete({ index: '_all' })
  .then(() => {
    console.log("all indices deleted");
    process.exit(0);
  })
  .catch((err) => {
    if (err.statusCode !== 404) {
      console.log("ERROR removing indices", err);
      process.exit(1);
    }
  });
