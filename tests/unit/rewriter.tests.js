const expect   = require('chai').expect;
const rewriter = require('../../lib/rewriter');
const errors   = require('../../lib/errors');

function itShouldRewriteAs(repr, expectedEsQuery, rewriterOpts = {}) {
  it(`rewrite: \'${JSON.stringify(repr)}\' correctly`, () => {
    const esQuery = rewriter.rewrite(rewriterOpts, repr);
    expect(esQuery).to.deep.equal(expectedEsQuery);
  });
}

function itShouldRejectUnsupportedQueryType(repr, rewriterOpts = {}) {
  it(`throw UnsuportedQueryType: \'${JSON.stringify(repr)}\'`, () => {
    const rewrite = () => rewriter.rewrite(rewriterOpts, repr);

    expect(rewrite).to.throw(rewriter.UnsupportedQueryTypeError);
  });
}

describe("rewriter unit tests", () => {
  describe("match none query", () => {
    itShouldRewriteAs(
      { query_type: 'match_none' },
      { match_none: {} }
    );
  });

  describe("match query", () => {
    itShouldRewriteAs(
      { query_type: 'match_phrase', field: 'x', value: 'foo' },
      { match_phrase: { x: 'foo' } }
    );

    itShouldRewriteAs(
      { query_type: 'match_phrase', field: 'x.d', value: 'foo' },
      { match_phrase: { 'x.d': 'foo' } }
    );

    itShouldRewriteAs(
      { query_type: 'match_phrase', field: 'book.\\*', value: 'foo' },
      { match_phrase: { 'book.\\*': 'foo' } }
    );
  });

  describe("wildcard queries", () => {
    itShouldRewriteAs(
      { query_type: 'wildcard', field: 'book', value: 'foo*' },
      { wildcard: { 'book': 'foo*' } }
    );
  });

  describe("rejected query types", () => {
    itShouldRejectUnsupportedQueryType(
      { query_type: 'all', value: 'foo' }
    );
  });

  describe('default operator', () => {
    itShouldRewriteAs(
      {
        query_type: 'default_operator',
        value: [
          { query_type: 'match', field: 'x', value: 1 },
          { query_type: 'match', field: 'y', value: 3 },
          { query_type: 'match', field: 'z', value: 3 },
          { query_type: 'match', field: 'x2', value: 3 }
        ]
      },
      {
        bool: {
          should: [
            { match: { x: 1 } },
            { match: { y: 3 } },
            { match: { z: 3 } },
            { match: { x2: 3 } }
          ]
        }
      }
    );

    itShouldRewriteAs(
      {
        query_type: 'default_operator',
        value: [
          { query_type: 'match', field: 'x', value: 1 },
          { query_type: 'match', field: 'y', value: 3 },
          { query_type: 'match', field: 'z', value: 3 },
          { query_type: 'match', field: 'x2', value: 3 }
        ]
      },
      {
        bool: {
          must: [
            { match: { x: 1 } },
            { match: { y: 3 } },
            { match: { z: 3 } },
            { match: { x2: 3 } }
          ]
        }
      },
      { default_operator: 'and' }
    );
  });

  describe("grouping", () => {
    itShouldRewriteAs(
      {
        query_type: 'default_operator',
        value: [
          { query_type: 'match', field: 'x', value: 1 }
        ]
      },
      { match: { x: 1 } }
    );

    itShouldRewriteAs(
      {
        query_type: 'default_operator',
        value: [
          { query_type: 'match', field: 'x', value: 1 },
          { query_type: 'match', field: 'y', value: 3 },
          { query_type: 'match', field: 'z', value: 3 },
          { query_type: 'match', field: 'x2', value: 3 }
        ]
      },
      {
        bool: {
          should: [
            { match: { x: 1 } },
            { match: { y: 3 } },
            { match: { z: 3 } },
            { match: { x2: 3 } }
          ]
        }
      }
    );

    itShouldRewriteAs(
      {
        query_type: 'default_operator',
        value: [
          { query_type: 'match', field: 'x', value: 1 },
          { query_type: 'match', field: 'y', value: 3 },
          { query_type: 'default_operator',
            value: [
              { query_type: 'match', field: 'z', value: 3 },
              { query_type: 'match', field: 'x2', value: 3 }
            ]
          }
        ]
      },
      {
        bool: {
          should: [
            { match: { x: 1 } },
            { match: { y: 3 } },
            {
              bool: {
                should: [
                  { match: { z: 3 } }, { match: { x2: 3 } }
                ]
              }
            }
          ]
        }
      }
    );

    itShouldRewriteAs(
      {
        query_type: 'default_operator',
        value: [
          {
            query_type: 'default_operator',
            value: [
              { query_type: 'match', field: 'x', value: 1 },
              { query_type: 'match', field: 'y', value: 3 } ]
          },
          {
            query_type: 'default_operator',
            value: [
              { query_type: 'match', field: 'z', value: 3 },
              { query_type: 'match', field: 'x2', value: 3 }
            ]
          }
        ]
      },
      {
        bool: {
          should: [
            { bool: { should: [ { match: { x: 1 } }, { match: { y: 3 } } ] } },
            { bool: { should: [ { match: { z: 3 } }, { match: { x2: 3 } } ] } }
          ]
        }
      }
    );
  });

  describe("boolean operators", () => {
    itShouldRewriteAs(
      {
        query_type: 'must',
        value: {
          query_type: 'match',
          field: 'x',
          value: 1
        }
      },
      {
        bool: {
          must: {
            match: { x: 1 }
          }
        }
      }
    );

    itShouldRewriteAs(
      {
        query_type: 'must_not',
        value: {
          query_type: 'match',
          field: 'x',
          value: 1
        }
      },
      {
        bool: {
          must_not: {
            match: { x: 1 }
          }
        }
      }
    );
  });

  describe("exists", () => {
    itShouldRewriteAs(
      { query_type: 'exists', field: 'foo' },
      { exists: { field: 'foo' } }
    );
  });

  describe("ranges", () => {
    itShouldRewriteAs(
      { query_type: 'range', field: 'x', gt: 1 },
      { range: { x: { gt: 1 } } }
    );

    itShouldRewriteAs(
      { query_type: 'range', field: 'x', gte: 1 },
      { range: { x: { gte: 1 } } }
    );

    itShouldRewriteAs(
      { query_type: 'range', field: 'x', lt: 1 },
      { range: { x: { lt: 1 } } }
    );

    itShouldRewriteAs(
      { query_type: 'range', field: 'x', lte: 1 },
      { range: { x: { lte: 1 } } }
    );

    itShouldRewriteAs(
      { query_type: 'range', field: 'x', gte: 1.3 },
      { range: { x: { gte: 1.3 } } }
    );

    itShouldRewriteAs(
      { query_type: 'range', field: 'count', gte: 2, lte: 5 },
      { range: { count: { gte: 2, lte: 5 } } }
    );

    itShouldRewriteAs(
      { query_type: 'range', field: 'count', gte: '2', lte: 'x' },
      { range: { count: { gte: '2', lte: 'x' } } }
    );

    itShouldRewriteAs(
      { query_type: 'range', field: 'count', gt: 2, lt: 5 },
      { range: { count: { gt: 2, lt: 5 } } }
    );

    itShouldRewriteAs(
      { query_type: 'range', field: 'count', gt: '2', lt: 'x' },
      { range: { count: { gt: '2', lt: 'x' } } }
    );
  });

  describe("regexp", () => {
    itShouldRewriteAs(
      { query_type: 'regexp', field: 'color', value: '/red/' },
      { regexp: { color: '/red/' } }
    );
  });

  describe("fuzziness", () => {
    itShouldRewriteAs(
      { query_type: 'fuzzy', field: 'color', value: 'red' },
      { fuzzy: { color: { value: 'red', fuzziness: 1 } } }
    );

    itShouldRewriteAs(
      {
        query_type: 'fuzzy',
        field: 'color',
        value: 'red',
        fuzziness: 2
      },
      { fuzzy: { color: { value: 'red', fuzziness: 2 } } }
    );
  });

  describe("proximity", () => {

    itShouldRewriteAs(
      {
        query_type: 'proximity',
        field: 'color',
        value: 'red foo',
        distance: 2
      },
      { match_phrase: { color: { query: 'red foo', sloop: 2 } } }
    );
  });

});
