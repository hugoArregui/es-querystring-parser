const expect = require('chai').expect;
const parser = require('../../lib/parser');
const errors = require('../../lib/errors');

function itShouldParseAs(queryString, expectedRepr) {
  it(`parse: \'${queryString}\' correctly`, () => {
    const repr = parser.parse(queryString);
    console.log(repr);
    expect(repr).to.be.deep.equal(expectedRepr);
  });
}

describe("parser unit tests", () => {

  describe('unsupported query', () => {
    it('should throw UnsupportedQueryError', () => {
      expect(() => parser.parse("+")).to.throw(errors.UnsupportedQueryError);
    });
  });

  describe("match query", () => {
    itShouldParseAs('', {
      query_type: 'match_none'
    });
  });

  describe("match query", () => {

    itShouldParseAs('x : "foo"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match_phrase', field: 'x', value: 'foo' }
      ]
    });

    itShouldParseAs(' x : "foo"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match_phrase', field: 'x', value: 'foo' }
      ]
    });

    itShouldParseAs('x:"foo"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match_phrase', field: 'x', value: 'foo' }
      ]
    });

    itShouldParseAs('x.d : "foo"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match_phrase', field: 'x.d', value: 'foo' }
      ]
    });

    itShouldParseAs('x.d.c : "foo"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match_phrase', field: 'x.d.c', value: 'foo' }
      ]
    });

    itShouldParseAs('x : 1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'x', value: 1 }
      ]
    });

    itShouldParseAs('x: 1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'x', value: 1 }
      ]
    });

    itShouldParseAs('x:1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'x', value: 1 }
      ]
    });

    itShouldParseAs('book.\\*:1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'book.\\*', value: 1 }
      ]
    });

    itShouldParseAs('x : john_2@gmail.com', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'x', value: 'john_2@gmail.com' }
      ]
    });
  });

  describe("wildcard query", () => {

    itShouldParseAs('x : *a', {
      query_type: 'default_operator',
      value: [
        { query_type: 'wildcard', field: 'x', value: '*a' }
      ]
    });

    itShouldParseAs(' x : a*a*', {
      query_type: 'default_operator',
      value: [
        { query_type: 'wildcard', field: 'x', value: 'a*a*' }
      ]
    });

    itShouldParseAs('x:foo*', {
      query_type: 'default_operator',
      value: [
        { query_type: 'wildcard', field: 'x', value: 'foo*' }
      ]
    });

    itShouldParseAs('x:foo?', {
      query_type: 'default_operator',
      value: [
        { query_type: 'wildcard', field: 'x', value: 'foo?' }
      ]
    });

    itShouldParseAs('x:f?oo?', {
      query_type: 'default_operator',
      value: [
        { query_type: 'wildcard', field: 'x', value: 'f?oo?' }
      ]
    });

    itShouldParseAs('x:?oo?', {
      query_type: 'default_operator',
      value: [
        { query_type: 'wildcard', field: 'x', value: '?oo?' }
      ]
    });
  });

  describe("all query", () => {
    itShouldParseAs('"hello"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'all', value: "hello" }
      ]
    });

    itShouldParseAs('hello', {
      query_type: 'default_operator',
      value: [
        { query_type: 'all', value: "hello" }
      ]
    });

    itShouldParseAs('hello*', {
      query_type: 'default_operator',
      value: [
        { query_type: 'all', value: "hello*" }
      ]
    });

    itShouldParseAs('1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'all', value: 1 }
      ]
    });
  });

  describe('default operator', () => {
    itShouldParseAs('x: 1 y: 3', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'x', value: 1 },
        { query_type: 'match', field: 'y', value: 3 }
      ]
    });

    itShouldParseAs('x: 1 y: 3 z: 3', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'x', value: 1 },
        { query_type: 'match', field: 'y', value: 3 },
        { query_type: 'match', field: 'z', value: 3 }
      ]
    });

    itShouldParseAs('x: 1 y: 3 z: 3 x2: 3', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'x', value: 1 },
        { query_type: 'match', field: 'y', value: 3 },
        { query_type: 'match', field: 'z', value: 3 },
        { query_type: 'match', field: 'x2', value: 3 }
      ]
    });
  });

  describe("grouping", () => {
    itShouldParseAs('(x: 1)', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'default_operator',
          value: [
            { query_type: 'match', field: 'x', value: 1 }
          ]
        }
      ]
    });

    itShouldParseAs('(x: 1 y: 3 z: 3 x2: 3)', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'default_operator',
          value: [
            { query_type: 'match', field: 'x', value: 1 },
            { query_type: 'match', field: 'y', value: 3 },
            { query_type: 'match', field: 'z', value: 3 },
            { query_type: 'match', field: 'x2', value: 3 }
          ]
        }
      ]
    });

    itShouldParseAs('(x: 1 y: 3 (z: 3 x2: 3))', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'default_operator',
          value: [
            { query_type: 'match', field: 'x', value: 1 },
            { query_type: 'match', field: 'y', value: 3 },
            { query_type: 'default_operator',
              value: [
                { query_type: 'match', field: 'z', value: 3 },
                { query_type: 'match', field: 'x2', value: 3 }
              ]
            }
          ]
        }
      ]
    });

    itShouldParseAs('(x: 1 y: 3) (z: 3 x2: 3)', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'default_operator',
          value: [
            { query_type: 'match', field: 'x', value: 1 },
            { query_type: 'match', field: 'y', value: 3 } ]
        },
        {
          query_type: 'default_operator',
          value: [
            { query_type: 'match', field: 'z', value: 3 },
            { query_type: 'match', field: 'x2', value: 3 }
          ]
        }
      ]
    });
  });

  describe("boolean operators", () => {
    itShouldParseAs('+x:1', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'must',
          value: {
            query_type: 'match',
            field: 'x',
            value: 1
          }
        }
      ]
    });

    itShouldParseAs('-x:1', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'must_not',
          value: {
            query_type: 'match',
            field: 'x',
            value: 1
          }
        }
      ]
    });

    itShouldParseAs('-(x: 1)', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'must_not',
          value: {
            query_type: 'default_operator',
            value: [
              { query_type: 'match', field: 'x', value: 1 }
            ]
          }
        }
      ]
    });
  });

  describe("exists", () => {
    itShouldParseAs('_exists_: "foo"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'exists', field: 'foo' }
      ]
    });

    itShouldParseAs('_exists_ : "foo"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'exists', field: 'foo' }
      ]
    });

    itShouldParseAs('_exists_: foo', {
      query_type: 'default_operator',
      value: [
        { query_type: 'exists', field: 'foo' }
      ]
    });
  });

  describe("comparators", () => {
    itShouldParseAs('x:>1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'x', gt: 1 }
      ]
    });

    itShouldParseAs('x:>= 1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'x', gte: 1 }
      ]
    });

    itShouldParseAs('x:< 1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'x', lt: 1 }
      ]
    });

    itShouldParseAs('x:>=1', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'x', gte: 1 }
      ]
    });

    itShouldParseAs('x:>=1.3', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'x', gte: 1.3 }
      ]
    });
  });

  describe("ranges", () => {
    itShouldParseAs('count:[2 TO 5]', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'count', gte: 2, lte: 5 }
      ]
    });

    itShouldParseAs('count:[ 2 TO   5]', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'count', gte: 2, lte: 5 }
      ]
    });

    itShouldParseAs('count:[ "2" TO   "x"]', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'count', gte: '2', lte: 'x' }
      ]
    });

    itShouldParseAs('count:{2 TO 5}', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'count', gt: 2, lt: 5 }
      ]
    });

    itShouldParseAs('count:{ 2 TO   5}', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'count', gt: 2, lt: 5 }
      ]
    });

    itShouldParseAs('count:{ 2 TO   5]', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'count', gt: 2, lte: 5 }
      ]
    });

    itShouldParseAs('count:{ "2" TO   "x"}', {
      query_type: 'default_operator',
      value: [
        { query_type: 'range', field: 'count', gt: '2', lt: 'x' }
      ]
    });

    itShouldParseAs('created_at:[2017-11-10 TO 2017-11-17]', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'range',
          field: 'created_at',
          gte: '2017-11-10',
          lte: '2017-11-17'
        }
      ]
    });

    itShouldParseAs('created_at:[2017-11-10T00:36:58 TO 2017-11-17T00:36:58]', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'range',
          field: 'created_at',
          gte: '2017-11-10T00:36:58',
          lte: '2017-11-17T00:36:58'
        }
      ]
    });
  });

  describe("and/or", () => {
    itShouldParseAs('count:2 AND color:"red"', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'and',
          value: [
            { query_type: 'match', field: 'count', value: 2 },
            { query_type: 'match_phrase', field: 'color', value: 'red' }
          ]
        }
      ]
    });

    itShouldParseAs('count:2 AND color:"red" AND _exists_: foo', {
      query_type: 'default_operator',
      value: [
        { query_type: 'and',
          value: [
            { query_type: 'match', field: 'count', value: 2 },
            { query_type: 'match_phrase', field: 'color', value: 'red' },
            { query_type: 'exists', field: 'foo' }
          ]
        }
      ]
    });

    itShouldParseAs('count:2 OR color:"red"', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'or',
          value: [
            { query_type: 'match', field: 'count', value: 2 },
            { query_type: 'match_phrase', field: 'color', value: 'red' }
          ]
        }
      ]
    });

    itShouldParseAs('count:2 OR color:"red" OR _exists_: foo', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'or',
          value: [
            { query_type: 'match', field: 'count', value: 2 },
            { query_type: 'match_phrase', field: 'color', value: 'red' },
            { query_type: 'exists', field: 'foo' }
          ]
        }
      ]
    });

    itShouldParseAs('count:2 AND color:"red" OR _exists_: foo', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'and',
          value: [
            { query_type: 'match', field: 'count', value: 2 },
            { query_type: 'or',
              value: [
                { query_type: 'match_phrase', field: 'color', value: 'red' },
                { query_type: 'exists', field: 'foo' }
              ]
            }
          ]
        }
      ]
    });

    itShouldParseAs('count:2 OR color:"red" AND _exists_: foo', {
      query_type: 'default_operator',
      value: [
        {
          query_type: 'and',
          value: [
            {
              query_type: 'or',
              value: [
                { query_type: 'match', field: 'count', value: 2 },
                { query_type: 'match_phrase', field: 'color', value: 'red' }
              ]
            },
            { query_type: 'exists', field: 'foo' }
          ]
        }
      ]
    });
  });

  describe("regexp", () => {
    itShouldParseAs('color:/red/', {
      query_type: 'default_operator',
      value: [
        { query_type: 'regexp', field: 'color', value: '/red/' }
      ]
    });

    itShouldParseAs('color.value:/red/', {
      query_type: 'default_operator',
      value: [
        { query_type: 'regexp', field: 'color.value', value: '/red/' }
      ]
    });
  });

  describe("fuzziness", () => {
    itShouldParseAs('color:red~', {
      query_type: 'default_operator',
      value: [
        { query_type: 'fuzzy', field: 'color', value: 'red' }
      ]
    });

    itShouldParseAs('color:red~2', {
      query_type: 'default_operator',
      value:
      [
        {
          query_type: 'fuzzy',
          field: 'color',
          value: 'red',
          fuzziness: 2
        }
      ]
    });
  });

  describe("proximity", () => {
    itShouldParseAs('color:"red foo"~2', {
      query_type: 'default_operator',
      value:
      [
        {
          query_type: 'proximity',
          field: 'color',
          value: 'red foo',
          distance: 2
        }
      ]
    });
  });


  describe("misc", () => {
    itShouldParseAs('color:"red","blue"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match_phrase', field: 'color', value: 'red' },
        { query_type: 'all', value: ',' },
        { query_type: 'all', value: 'blue' }
      ]
    });

    itShouldParseAs('a\\ name: "foo"', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match_phrase', field: 'a\\ name', value: 'foo' }
      ]
    });

    itShouldParseAs('email:ANDREA', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'email', value: 'ANDREA' }
      ]
    });

    itShouldParseAs('user_id:2d1ced61\\-1636', {
      query_type: 'default_operator',
      value: [
        { query_type: 'match', field: 'user_id', value: '2d1ced61\\-1636' }
      ]
    });
  });
});
