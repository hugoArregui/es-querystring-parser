const fs     = require("fs");
const Parser = require("jison").Parser;
const grammar = require('../lib/grammar');

const parser = new Parser(grammar);

const parserSource = parser.generate();

fs.writeFileSync("parser.js", parserSource, "utf8");
