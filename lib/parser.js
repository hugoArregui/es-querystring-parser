const parser = require('../parser');
const errors = require('./errors');

module.exports = {
  parse: (qs) => {
    if (qs === '') {
      return { 'query_type': 'match_none' };
    }

    try {
      return parser.parse(qs);
    } catch(e) {
      throw new errors.UnsupportedQueryError(qs, e);
    }
  }
};
