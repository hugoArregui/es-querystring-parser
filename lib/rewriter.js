const errors = require('./errors');

function rewriteMatchQuery(expr) {
  const r = {
    match: {}
  };
  r.match[expr.field] = expr.value;
  return r;
}

function rewriteMatchPhraseQuery(expr) {
  const r = {
    match_phrase: {}
  };
  r.match_phrase[expr.field] = expr.value;
  return r;
}

function rewriteRangeQuery(expr) {
  const r = {
    range: {}
  };
  const condition = {};

  if (expr.gt) {
    condition.gt = expr.gt;
  } else if (expr.gte) {
    condition.gte = expr.gte;
  }

  if (expr.lt) {
    condition.lt = expr.lt;
  } else if (expr.lte) {
    condition.lte = expr.lte;
  }

  r.range[expr.field] = condition;
  return r;
}

function rewriteExistsQuery(expr) {
  return {
    exists: {
      field: expr.field
    }
  };
}

function rewriteBooleanQuery(expr, rewrite, default_operator) {
  const r = {
    bool: {}
  };

  const clauses = expr.value
        .map((e) => rewrite(e, r))
        .filter((c) => c);

  if (clauses.length > 0) {
    if (default_operator === 'or') {
      r.bool.should = clauses;
    } else {
      r.bool.must = clauses;
    }
  }

  return r;
}

function rewriteMustQuery(expr, rewrite, parent) {
  if (parent && parent.bool) {
    parent.bool.must = parent.bool.must || [];
    parent.bool.must.push(rewrite(expr.value));
    return false;
  } else {
    const r = {
      bool: {}
    };

    r.bool.must = rewrite(expr.value);

    return r;
  }
}

function rewriteMustNotQuery(expr, rewrite, parent) {
  if (parent && parent.bool) {
    parent.bool.must_not = parent.bool.must_not || [];
    parent.bool.must_not.push(rewrite(expr.value));
    return false;
  } else {
    const r = {
      bool: {}
    };

    r.bool.must_not = rewrite(expr.value);

    return r;
  }
}

function rewriteRegexpQuery(expr) {
  const r = {
    regexp: {}
  };

  r.regexp[expr.field] = expr.value;
  return r;
}

function rewriteFuzzyQuery(expr) {
  const r = {
    fuzzy: {}
  };

  r.fuzzy[expr.field] = {
    value: expr.value,
    fuzziness: expr.fuzziness || 1
  };

  return r;
}

function rewriteProximityQuery(expr) {
  const r = {
    match_phrase: {}
  };

  r.match_phrase[expr.field] = {
    query: expr.value,
    sloop: expr.distance
  };

  return r;
}

function rewriteWildcardQuery(expr) {
  const r = {
    wildcard: {}
  };

  r.wildcard[expr.field] = expr.value;

  return r;
}

function rewriteMatchNoneQuery() {
  return {  match_none: {} };
}

function rewrite(opts, expr, parent) {
  const _rewrite = (expr, parent) => rewrite(opts, expr, parent);

  opts.default_operator = opts.default_operator || 'or';

  if (!expr.query_type) {
    throw new errors.InvalidRepresentationError(expr);
  }

  if (opts.transform) {
    expr = opts.transform(expr);
  }

  switch (expr.query_type) {
  case 'default_operator':
    if (expr.value.length === 1) {
      return _rewrite(expr.value[0]);
    } else {
      return rewriteBooleanQuery(expr, _rewrite, opts.default_operator);
    }
    break;
  case 'or':
    if (expr.value.length === 1) {
      return _rewrite(expr.value[0]);
    } else {
      return rewriteBooleanQuery(expr, _rewrite, 'or');
    }
    break;
  case 'and':
    if (expr.value.length === 1) {
      return _rewrite(expr.value[0]);
    } else {
      return rewriteBooleanQuery(expr, _rewrite, 'and');
    }
    break;
  case 'match':
    return rewriteMatchQuery(expr);
  case 'match_phrase':
    return rewriteMatchPhraseQuery(expr);
  case 'range':
    return rewriteRangeQuery(expr);
  case 'exists':
    return rewriteExistsQuery(expr);
  case 'must':
    return rewriteMustQuery(expr, _rewrite, parent);
  case 'must_not':
    return rewriteMustNotQuery(expr, _rewrite, parent);
  case 'regexp':
    return rewriteRegexpQuery(expr);
  case 'fuzzy':
    return rewriteFuzzyQuery(expr);
  case 'proximity':
    return rewriteProximityQuery(expr);
  case 'wildcard':
    return rewriteWildcardQuery(expr);
  case 'match_none':
    return rewriteMatchNoneQuery(expr);
  default:
    throw new errors.UnsupportedQueryType(expr.query_type, expr);
  }
}

module.exports = {
  rewrite
};
