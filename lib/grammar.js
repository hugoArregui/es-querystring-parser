// IMPORTANT:
// Ref: https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html

const reserved_chars = '\\|\\-+*"()~/\\[\\]{}><=:\\s\\\\';
const escaped_chars = '\\\\ |\\\\\\*|\\\\-';
const valid_chars = `[^${reserved_chars}]|${escaped_chars}`;

module.exports = {
  "lex": {
    "macros": {
      "int": "-?(?:[0-9]|[1-9][0-9]+)",
      "exp": "(?:[eE][-+]?[0-9]+)",
      "frac": "(?:\\.[0-9]+)",
      "valid_chars": valid_chars,
      "keyword": "{valid_chars}+",
      "keyword_with_wildcard": "(?:{valid_chars}+\\*|\\*{valid_chars}+|{valid_chars}+\\?|\\?{valid_chars}+)+\\**\\?*",
      "regexp": "(?:\/.+\/)",
      "string": '\\"[^\\"]*\\"',
      "date": "[0-9]{4}-[0-9]{2}-[0-9]{2}",
      "date_time": "{date}T[0-9]{2}:[0-9]{2}:[0-9]{2}",
    },
    "rules": [
      ["_exists_\\s*:",           "return 'EXISTS'"],
      [
        "{keyword}\\s*:",
        "yytext = yytext.substr(0,yyleng-1).trim(); return 'FIELDNAME_SPEC';"
      ],
      [" TO ",                    "return 'TO'"],
      [" OR ",                    "return 'OR'"],
      [" AND ",                   "return 'AND'"],
      ["\\s+",                    "/* skip whitespace */"],
      ["{date_time}",             "return 'DATE'"],
      ["{date}",                  "return 'DATE'"],
      ["{int}{frac}?{exp}?\\b",   "yytext = Number(yytext); return 'NUMBER';"],
      ["{keyword_with_wildcard}", "return 'KEYWORD_WITH_WILDCARD';"],
      ["{keyword}",               "return 'KEYWORD';"],
      [
        "{string}",
        "yytext = yytext.substr(1,yyleng-2); return 'STRING';"
      ],
      ["{regexp}",                "return 'REGEXP'"],
      ["\\(",                     "return '(';"],
      ["\\)",                     "return ')';"],
      ["\\[",                     "return '[';"],
      ["\\]",                     "return ']';"],
      ["\\{",                     "return '{';"],
      ["\\}",                     "return '}';"],
      ["\\+",                     "return '+';"],
      ["\\-",                     "return '-';"],
      ["\\>",                     "return '>';"],
      ["\\<",                     "return '<';"],
      ["\\=",                     "return '=';"],
      ["\\~",                     "return '~';"],
    ]
  },
  "start": "QueryString",
  "operators": [
    ["left", "AND"],
    ["left", "OR"]
  ],
  "bnf": {
    "QueryString": [
      ["QueryList", "return $$ = $1;"]
    ],
    "FieldValue": [
      "NUMBER",
      "STRING",
      "DATE"
    ],
    "Query": [
      [
        "FieldValue",
        "$$ = { query_type: 'all', value: $1 };"
      ],
      [
        "KEYWORD",
        "$$ = { query_type: 'all', value: $1 };"
      ],
      [
        "KEYWORD_WITH_WILDCARD",
        "$$ = { query_type: 'all', value: $1 };"
      ],
      [
        "FIELDNAME_SPEC KEYWORD_WITH_WILDCARD",
        "$$ = { query_type: 'wildcard', field: $1, value: $2 };"
      ],
      [
        "FIELDNAME_SPEC KEYWORD",
        "$$ = { query_type: 'match', field: $1, value: $2 };"
      ],
      [
        "FIELDNAME_SPEC NUMBER",
        "$$ = { query_type: 'match', field: $1, value: $2 };"
      ],
      [
        "FIELDNAME_SPEC STRING",
        "$$ = { query_type: 'match_phrase', field: $1, value: $2 };"
      ],
      [
        "FIELDNAME_SPEC KEYWORD ~",
        "$$ = { query_type: 'fuzzy', field: $1, value: $2 };"
      ],
      [
        "FIELDNAME_SPEC KEYWORD ~ NUMBER",
        "$$ = { query_type: 'fuzzy', field: $1, value: $2, fuzziness: $4 };"
      ],
      [
        "FIELDNAME_SPEC STRING ~ NUMBER",
        "$$ = { query_type: 'proximity', field: $1, value: $2, distance: $4 };"
      ],
      [
        "FIELDNAME_SPEC REGEXP",
        "$$ = { query_type: 'regexp', field: $1, value: $2 };"
      ],
      [
        "FIELDNAME_SPEC [ FieldValue TO FieldValue ]",
        "$$ = { query_type: 'range', field: $1, gte: $3, lte: $5 };"
      ],
      [
        "FIELDNAME_SPEC [ FieldValue TO FieldValue }",
        "$$ = { query_type: 'range', field: $1, gte: $3, lt: $5 };"
      ],
      [
        "FIELDNAME_SPEC { FieldValue TO FieldValue }",
        "$$ = { query_type: 'range', field: $1, gt: $3, lt: $5 };"
      ],
      [
        "FIELDNAME_SPEC { FieldValue TO FieldValue ]",
        "$$ = { query_type: 'range', field: $1, gt: $3, lte: $5 };"
      ],
      [
        "FIELDNAME_SPEC > FieldValue",
        "$$ = { query_type: 'range', field: $1, gt: $3 };"
      ],
      [
        "FIELDNAME_SPEC > = FieldValue",
        "$$ = { query_type: 'range', field: $1, gte: $4 };"
      ],
      [
        "FIELDNAME_SPEC < FieldValue",
        "$$ = { query_type: 'range', field: $1, lt: $3 };"
      ],
      [
        "FIELDNAME_SPEC < = FieldValue",
        "$$ = { query_type: 'range', field: $1, lte: $4 };"
      ],
      [
        "EXISTS KEYWORD",
        "$$ = { query_type: 'exists', field: $2 };"
      ],
      [
        "EXISTS STRING",
        "$$ = { query_type: 'exists', field: $2 };"
      ],
      [
        "+ Query",
        "$$ = { query_type: 'must', value: $2 }"
      ],
      [
        "- Query",
        "$$ = { query_type: 'must_not', value: $2 }"
      ],
      [
        "( QueryList )",
        "$$ = $2"
      ],
      [
        "CompoundAndQuery",
        "$$ = $1"
      ],
      [
        "CompoundOrQuery",
        "$$ = $1"
      ],
    ],
    "QueryList": [
      [ "Query", "$$ = { query_type: 'default_operator', value: [$1] };" ],
      [ "QueryList Query", "$$ = $1; $1.value.push($2);" ]
    ],
    "CompoundAndQuery": [
      [ "Query AND Query", "$$ = { query_type: 'and', value: [$1, $3] };" ],
      [ "CompoundAndQuery AND Query", "$$ = $1; $1.value.push($3);" ]
    ],
    "CompoundOrQuery": [
      [ "Query OR Query", "$$ = { query_type: 'or', value: [$1, $3] };" ],
      [ "CompoundOrQuery OR Query", "$$ = $1; $1.value.push($3);" ]
    ]
  }
};
