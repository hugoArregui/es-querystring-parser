class UnsupportedQueryError extends Error {
  constructor(qs, e) {
    super(`Cannot parse: ${qs}`);
    this.originalError = e;
  }
}

class InvalidRepresentationError extends Error {
  constructor(repr) {
    super('Invalid intermediate representation');
    this.repr = repr;
  }
}

class UnsupportedQueryTypeError extends Error {
  constructor(type, expr) {
    super(`don't know how to rewrite query type '${type}'`);
    this.expr = expr;
  }
}

module.exports = {
  UnsupportedQueryError,
  UnsupportedQueryTypeError,
  InvalidRepresentationError
};
