const parser   = require('./lib/parser');
const rewriter = require('./lib/rewriter');
const errors   = require('./lib/errors');

module.exports = {
  parse: (input) => parser.parse(input),
  rewrite: (opts, repr) => rewriter.rewrite(opts, repr),
  errors
};
